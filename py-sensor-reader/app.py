#!/usr/bin/env python3

import PCF8591 as ADC
import RPi.GPIO as GPIO
import datetime
import time
import math
import pika
import json

GPIO.setmode(GPIO.BCM)

def setup():
    ADC.setup(0x48)
    GPIO.setup(17, GPIO.IN)

# convert timestamp and temp value to json key values
def format_message(temp):
    timestamp_raw = datetime.datetime.now()
    timestamp = timestamp_raw.strftime('%Y-%m-%dT%H:%M:%S')
    json_data = {"timestamp": timestamp, "temperature": temp}
    return json_data

# Collect temperature reading
def read_temp():
    analogVal = ADC.read(0)
    Vr = 5 * float(analogVal) / 255
    Rt = 10000 * Vr / (5 - Vr)
    temp = 1/(((math.log(Rt / 10000)) / 3950) + (1 / (273.15+25)))
    return temp

if __name__ == '__main__':
    try:
        setup()
        count = 0
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq'))
        channel = connection.channel()
        channel.queue_declare(queue='temp_raw')
        while True:
            temp = read_temp()
            json_data = format_message(temp)
            channel.basic_publish(exchange='', routing_key='temp_raw', body=json.dumps(json_data))
            print (str(count) + 'records published to rabbitmq so far\n\n')
            count += 1
#           time.sleep(0.1)
        connection.close()
    except KeyboardInterrupt:
        pass
