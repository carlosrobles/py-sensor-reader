from arm32v7/python:slim

MAINTAINER carlos.robles@gmail.com

COPY Pipfile Pipfile.lock /app/

WORKDIR /app

RUN apt-get update && \
    apt-get install -y gcc && \
    pip install pipenv && \
    pipenv install --deploy --system

COPY py-sensor-reader /app/

CMD tail -f /dev/null
